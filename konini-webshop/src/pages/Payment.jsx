import React, { useState, useEffect } from 'react';
import Visa from '../assets/visa.png';
import Mastercard from '../assets/mastercard.png';
import Postnord from '../assets/postnord.png';
import Dhl from '../assets/dhl.png';
import { Link } from 'react-router-dom';

const Payment = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  
  const handleOptionChange = (option) => {
    setSelectedOption(option);
    setIsOpen(false);
  };
  
  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };
  
  
  const [isOpenDropdown2, setIsOpenDropdown2] = useState(false);
  const [selectedOptionDropdown2, setSelectedOptionDropdown2] = useState(null);
  
  const handleOptionChangeDropdown2 = (option) => {
    setSelectedOptionDropdown2(option);
    setIsOpenDropdown2(false);
  };
  
  const toggleDropdown2 = () => {
    setIsOpenDropdown2(!isOpenDropdown2);
  };
  
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  return (
    <div className='max-w-screen-lg mx-auto pb-8'>
      <div className="max-w-screen-lg mx-auto pb-8">
        <h1 className="text-black text-2xl text-center my-10">Kassa</h1>
      </div>
      <h2 className='text-xl mb-4 pt-24'>3. Betalningssätt</h2>
      <div className=" mt-24 relative flex flex-col items-center">
        <button
          type="button"
          className="w-96 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded-l inline-flex items-center mb-2"
          onClick={toggleDropdown}
        >
          <p className='text-black font-semibold text-lg'>Kontokort</p>
          <div className='ml-64'>
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24">
              <path d="M7.33 24l-2.83-2.829 9.339-9.175-9.339-9.167 2.83-2.829 12.17 11.996z" />
            </svg>
          </div>
        </button>

        {isOpen && (
          <div
            className="right-0 w-96 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 border border-gray-400 mb-7"
            role="menu"
            aria-orientation="vertical"
          >
            <div className="py-1" role="none">
              <p className='pl-5 text-black font-semibold text-lg p-3 cursor-pointer border-b border-gray-300'>Kontokort</p>
              <label
                className="flex items-center p-2 hover:bg-gray-100 cursor-pointer border-b border-gray-300"
                onClick={() => handleOptionChange('Visa')}
              >
                <img
                  src={Visa}
                  alt="visa"
                  className="ml-2 w-8 h-8 rounded-full"
                />
                <span className="ml-5 text-black font-bold w-full">XXXX Bank</span>
                <span className="ml-2 text-black font-bold w-full">**** **** ****</span>
                <input
                  type="radio"
                  className="h-4 w-4 text-blue-600 border-gray-300 focus:ring-blue-500"
                  checked={selectedOption === 'Visa'}
                  readOnly
                />
              </label>

              <label
                className="flex items-center p-2 hover:bg-gray-100 cursor-pointer border-b border-gray-300 "
                onClick={() => handleOptionChange('Mastercard')}
              >
                <img
                  src={Mastercard}
                  alt="mastercard"
                  className="ml-2 w-8 h-8 rounded-full"
                />
                <span className="ml-5 text-black font-bold w-full">XXXX Bank</span>
                <span className="ml-2 text-black font-bold w-full">**** **** ****</span>
                <input
                  type="radio"
                  className="h-4 w-4 text-blue-600 border-gray-300 focus:ring-blue-500"
                  checked={selectedOption === 'mastercard'}
                  readOnly
                />
              </label>
            </div>
          </div>
        )}
      </div>

      <div className="relative flex flex-col items-center">
        <button
          type="button"
          className="w-96 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded-l inline-flex items-center mb-2"
          onClick={toggleDropdown2}
        >
          <p className='text-black font-semibold text-lg'>Klarna</p>
          <div className='ml-72'>
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24">
              <path d="M7.33 24l-2.83-2.829 9.339-9.175-9.339-9.167 2.83-2.829 12.17 11.996z" />
            </svg>
          </div>
        </button>

        {isOpenDropdown2 && (
          <div
            className="right-0 w-96 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 border border-gray-400"
            role="menu"
            aria-orientation="vertical"
          >
            <div className="py-1" role="none">
              <p className='pl-5 text-black font-semibold text-lg p-3 cursor-pointer border-b border-gray-300'>Klarna</p>

              <label
                className="flex items-center p-2 hover:bg-gray-100 cursor-pointer border-b border-gray-300"
                onClick={() => handleOptionChangeDropdown2('klarna')}
              >
                <div className="ml-2 w-8 h-8 rounded-full mb-2">
                  <svg xmlns="http://www.w3.org/2000/svg" width="100" height="40.4494" viewBox="0 0 100 40.4494">
                    <rect fill="#ffb3c7" width="100" height="40.4494" rx="11.7384" />
                    <path d="M80.0772,17.7235a6.194,6.194,0,1,0,0,10.2373v.75h3.5145V16.9727H80.0772Zm-3.1964,8.1388a3.0243,3.0243,0,1,1,3.1849-3.02A3.1059,3.1059,0,0,1,76.8808,25.8623Z" fill="#000" />
                    <rect x="28.2027" y="11.7416" width="3.678" height="16.9694" fill="#000" />
                    <path d="M64.1542,16.6569a4.2694,4.2694,0,0,0-3.62,1.6383V16.9733h-3.5V28.711h3.5421V22.5424a2.4754,2.4754,0,0,1,2.6383-2.6591c1.5447,0,2.4325.9228,2.4325,2.6348V28.711h3.5107V21.2464C69.1577,18.5146,66.9857,16.6569,64.1542,16.6569Z" fill="#000" />
                    <path d="M43,17.7235a6.194,6.194,0,1,0,0,10.2373v.75h3.5145V16.9727H43Zm-3.1965,8.1388a3.0243,3.0243,0,1,1,3.1845-3.02A3.1058,3.1058,0,0,1,39.8034,25.8623Z" fill="#000" />
                    <path d="M51.9513,18.5017V16.9728H48.353V28.7111h3.606v-5.48c0-1.8491,2.0042-2.843,3.3949-2.843.014,0,.0276.0014.0417.0015V16.9735A4.3821,4.3821,0,0,0,51.9513,18.5017Z" fill="#000" />
                    <path d="M87.323,24.5476a2.2059,2.2059,0,1,0,2.206,2.2059A2.2057,2.2057,0,0,0,87.323,24.5476Z" fill="#000" />
                    <path d="M25.6675,11.7384H21.8558a9.7488,9.7488,0,0,1-3.9412,7.8678l-1.51,1.131,5.8513,7.9792h4.8106l-5.3837-7.342A13.5049,13.5049,0,0,0,25.6675,11.7384Z" fill="#000" />
                    <rect x="12.1204" y="11.7384" width="3.8185" height="16.9773" fill="#000" />
                  </svg>

                </div>


                <span className="ml-5 text-black font-bold w-full"></span>
                <input
                  type="radio"
                  id="radioOption3"
                  name="radioOptionsDropdown2"
                  className="h-4 w-4 text-blue-600 border-gray-300 focus:ring-blue-500"
                  checked={selectedOptionDropdown2 === 'klarna'}
                  readOnly
                />
              </label>
            </div>
          </div>
        )}
      </div>
      <h2 className='text-xl mb-24 pt-24'>4. Fraktsätt</h2>

      <div className="flex mx-auto w-1/2">
        <div className="flex items-center h-5">
          <input id="postnord" type="radio" value="postnord" className="w-4 h-4 text-blue-600 focus:ring-blue-500 dark:focus:ring-blue-600" />
        </div>
        <div className="ms-2 text-sm flex items-center">
          <img src={Postnord} alt="Postnord Logo" className="w-14 h-4 mr-2 mb-10" />
          <div>
            <label htmlFor="postnord" className="font-medium text-black">Postnord</label>
            <p id="postnord" className="text-xs font-normal text-black">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad mollitia eos modi assumenda a quo quibusdam architecto tenetur, dolorem enim deserunt fugit tempora maxime harum facilis! Sed suscipit placeat non.</p>
          </div>
        </div>
      </div>
      <div className="flex mx-auto w-1/2 mt-6">
        <div className="flex items-center h-5">
          <input id="postnord" type="radio" value="postnord" className="w-4 h-4 text-blue-600 focus:ring-blue-500 dark:focus:ring-blue-600 " />
        </div>
        <div className="ms-2 text-sm flex items-center">
          <img src={Postnord} alt="Postnord Logo" className="w-14 h-4 mr-2 mb-10" />
          <div>
            <label htmlFor="postnord" className="font-medium text-black">Postnord</label>
            <p id="postnord" className="text-xs font-normal text-black">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad mollitia eos modi assumenda a quo quibusdam architecto tenetur, dolorem enim deserunt fugit tempora maxime harum facilis! Sed suscipit placeat non.</p>
          </div>
        </div>
      </div>
      <div className="flex mx-auto w-1/2 mt-6">
        <div className="flex items-center h-5 ">
          <input id="dhl" type="radio" value="dhl" className="w-4 h-4 text-blue-600 focus:ring-blue-500 dark:focus:ring-blue-600 " />
        </div>
        <div className="ms-2 text-sm flex items-center">
          <img src={Dhl} alt="DHL Logo" className="w-14 h-8 mr-2 mb-9" />
          <div>
            <label htmlFor="dhl" className="font-medium text-black">DHL</label>
            <p id="dhl" className="text-xs font-normal text-black">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad mollitia eos modi assumenda a quo quibusdam architecto tenetur, dolorem enim deserunt fugit tempora maxime harum facilis! Sed suscipit placeat non.</p>
          </div>
        </div>
      </div>
      <div className="mt-24 flex justify-between mb-24">
        <Link to={{ pathname: "/ProductCatalogue" }}>
          <button className="bg-white text-dark-pink border border-dark-pink  rounded-full py-2 px-10 tracking-widest">
            GÅ TILLBAKA
          </button>
        </Link>

        <Link to={{ pathname: "/Confirmation" }}>
          <button className="bg-dark-pink text-white rounded-full py-2 px-10 tracking-widest">
            BETALA
          </button>
        </Link>
      </div>
    </div>
  );
};


export default Payment