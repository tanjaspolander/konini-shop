import React,{useEffect} from 'react';
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';

const Checkout = ({ addToCart, cartItems, products, count, calculateTotalQuantity, calculateTotalPrice, calculateQuantities }) => {
  const location = useLocation();
  const selectedProducts = location.state?.selectedProducts;



  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])


  return (
    <div className='h-screen '>
      <div className="max-w-screen-lg mx-auto pb-8">
        <h1 className="text-black text-2xl text-center my-10">Kassa</h1>
        <h2 className="text-xl mb-4">1. Din varukorg</h2>

        {selectedProducts?.map(({ product, quantity }) => {
          const totalQuantity = calculateTotalQuantity(product.id, quantity);
          const totalPrice = calculateTotalPrice(product.id, quantity, product);
        return product ? (
            <div key={product.id} className="grid grid-cols-4 gap-4 py-10 border-b-2 border-black">
              <div>
                <img
                  className="w-auto h-40"
                  src={process.env.PUBLIC_URL + '/' + product.images[1]}
                  alt={product.name}
                />
              </div>
      
              <div className="col-span-2">
                <h2 className="text-xl">{product.name}</h2>
              </div>

              <div>
              <p className="text-black text-lg">{totalPrice - (totalPrice/2)} kr</p>
              <p className="text-gray">Antal {totalQuantity - (totalQuantity/2)}</p>
              
              </div>
            </div>
            
          ) : null
          
        })}
                      
        <h2 className="text-xl mb-4 pt-24">2. Dina uppgifter</h2>
        <form>
          <div className="flex mb-4">
            <div className="w-1/2 mr-4">
              <label className="block text-sm font-medium text-gray-700">
                Förnamn <span className="text-red-400">*</span>
              </label>
              <input
                type="text"
                id="förnamn"
                name="förnamn"
                className="mt-1 p-2 border border-black rounded-2xl focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm"
              />
            </div>

            <div className="w-1/2">
              <label className="block text-sm font-medium text-gray-700">
                Efternamn <span className="text-red-400">*</span>
              </label>
              <input
                type="text"
                id="Efternamn"
                name="Efternamn"
                className="mt-1 p-2 border border-black rounded-2xl focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm"
              />
            </div>
          </div>

          <div className="flex mb-4">
            <div className="w-1/2 mr-4">
              <label className="block text-sm font-medium text-gray-700">
                Email <span className="text-red-400">*</span>
              </label>
              <input
                type="text"
                id="email"
                name="email"
                className="mt-1 p-2 border border-black rounded-2xl focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm"
              />
            </div>

            <div className="w-1/2">
              <label className="block text-sm font-medium text-gray-700">
                Telefonnummer <span className="text-red-400">*</span>
              </label>
              <input
                type="text"
                id="telefonnummer"
                name="telefonnummer"
                className="mt-1 p-2 border border-black rounded-2xl focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm"
              />
            </div>
          </div>

          <div className="mb-4">
            <label className="block text-sm font-medium text-gray-700">
              Adress <span className="text-red-400">*</span>
            </label>
            <input
              type="text"
              id="name5"
              name="name5"
              className="mt-1 p-2 border border-black rounded-2xl focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm"
            />
          </div>

          <div className="flex mb-4">
            <div className="w-1/2 mr-4">
              <label className="block text-sm font-medium text-gray-700">
                Postnummer <span className="text-red-400">*</span>
              </label>
              <input
                type="text"
                id="postnummer"
                name="postnummer"
                className="mt-1 p-2 border border-black rounded-2xl focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm"
              />
            </div>

            <div className="w-1/2">
              <label className="block text-sm font-medium text-gray-700">
                Stad <span className="text-red-400">*</span>
              </label>
              <input
                type="text"
                id="stad"
                name="stad"
                className="mt-1 p-2 border border-black rounded-2xl focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm"
              />
            </div>
          </div>
        </form>
        <div className="mt-24 flex justify-between mb-24">
          <Link to={{ pathname: "/ProductCatalogue" }}>
            <button className="bg-white text-dark-pink border border-dark-pink  rounded-full py-2 px-10 tracking-widest">
              GÅ TILLBAKA
            </button>
          </Link>

          <Link to={{ pathname: "/Payment" }}>
            <button className="bg-dark-pink text-white rounded-full py-2 px-10 tracking-widest">
              TILL KASSAN
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Checkout
