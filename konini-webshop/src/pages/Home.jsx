import React, {useEffect} from 'react'
import Header from '../components/Header'
import Banger from '../components/Banger'
import Info from '../components/Info'
import Aside from '../components/Aside'
import Newsletter from '../components/Newsletter'

const Home = () => {

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  return (
    <div>
    <Header/>
    <Banger />
    <Info />
    <Aside />
    <Newsletter />
    </div>
  )
}

export default Home