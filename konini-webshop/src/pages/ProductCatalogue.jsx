import React, {useEffect} from 'react';
import { Link } from 'react-router-dom';
import SlideOverCart from '../components/SlideOverCart';
import Newsletter from '../components/Newsletter';
import { LiaShoppingBagSolid } from "react-icons/lia";


//ProductCatalogue är en komponent som hämtar datan från json filen och lagrar den i en array.
const ProductCatalogue = ({ addToCart, cartIsOpen, setCartIsOpen, count, increment, decrement, products, cartItems, calculateTotalPrice, calculateTotalQuantity, calculateQuantities}) => {
  
    //products, arrayen som lagrar datan från json filen.
  

    const closeCart = () => {
        setCartIsOpen(false);
    };

    useEffect(() => {
      window.scrollTo(0, 0)
    }, [])
    
    return (
<div>
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 p-4">
  {products.map((product) => (
    <div key={product.id} className="bg-white p-1 flex flex-col">
      <Link to={`/product/${product.id}`} state={{ product }}>
        <img src={product.images[0]} alt={product.name} className="mb-2 " />
      </Link>
      <h2 className="text-lg font-bold mb-1">{product.name}</h2>
      <p className="text-gray-500">${product.price}</p>
      <button
        className="bg-white text-black border border-black rounded-full px-4 py-1 mt-2 self-end flex items-center text-sm"
        onClick={() => addToCart(product.id)}
      >
        Lägg till <LiaShoppingBagSolid />
      </button>
    </div>
  ))}
  {cartIsOpen && (
    <SlideOverCart cartItems={cartItems} products={products} closeCart={closeCart} count={count} increment={increment} decrement={decrement} calculateTotalQuantity={calculateTotalQuantity} calculateTotalPrice={calculateTotalPrice} calculateQuantities={calculateQuantities} />
  )}</div>
      <div className="grid grid-cols-1">
        <Newsletter />
      </div>
    </div>
    );
  };

export default ProductCatalogue;
