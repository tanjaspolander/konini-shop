import React, {useEffect} from 'react';
import { Link } from 'react-router-dom';

const Confirmation = () => {

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
      
    return (
        <div className="min-h-screen flex items-center justify-center bg-gray-100">
            <div className="text-center">
                <h1 className="text-3xl font-bold text-green-600 mb-4">Tack för din beställning!</h1>
                <h2 className="text-lg text-gray-700 mb-8">En orderbekräftelse har skickats till din mailadress. Tack för att du shoppat hos Konini Shop.</h2>

                <Link to={{ pathname: "/ProductCatalogue" }}>
                <button className="bg-dark-pink tracking-widest text-white px-6 py-3 rounded-full hover:bg-blue-600">          TILLBAKA TILL AFFÄREN        </button>
                </Link>
            </div>
        </div>
    );
};

export default Confirmation;