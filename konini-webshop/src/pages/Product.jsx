import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import SlideOverCart from '../components/SlideOverCart';
import Modal from '../components/modal';
import { LiaRulerHorizontalSolid } from "react-icons/lia";


const Product = ({ cartIsOpen, setCartIsOpen, products, count, cartItems, setCartItems, increment, decrement, calculateTotalPrice, calculateTotalQuantity, calculateQuantities }) => {
  const [selectedSize, setSelectedSize] = useState('');
  const [selectedQuantity, setSelectedQuantity] = useState(1);
  const [isSizeGuideOpen, setIsSizeGuideOpen] = useState(false);
  const { state } = useLocation();
  const product = state ? state.product : null;

  const handleSizeChange = (event) => {
    setSelectedSize(event.target.value);
  };

  const handleQuantityChange = (event) => {
    setSelectedQuantity(event.target.value);
  };

  const closeCart = () => {
    setCartIsOpen(false);
  };

  const addToCart = (productId) => {
    setCartItems([...cartItems, productId]);
    setCartIsOpen(true);
    console.log(cartItems);
  };

  const sizes = ['XS', 'S', 'M', 'L', 'XL'];
  const quantities = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  return (
    <div className="bg-gray-100 min-h-screen">
      <div className="p-8 flex">
        {/* bilden visas bara med denna lösning varför? */}
        <div className="w-1/2">
          {product.images && product.images[1] && (
            <img className="w-full" src={process.env.PUBLIC_URL + '/' + product.images[1]} alt={product.name} />
          )}
        </div>
        <div className="w-1/2 pl-8">
          <h1 className="text-2xl font-bold mb-4">{product.name}</h1>
          <p className="text-gray-600 mb-4">{product.description}</p>
          <div className="flex items-center justify-between mb-4">
            <span className="text-lg font-bold">${product.price}</span>
          </div>
          <div className="mb-4">
            <div>
              {sizes.map((size) => (
                <label htmlFor={`size${size}`} className="mr-2" key={size}>
                  <input
                    type="radio"
                    id={`size${size}`}
                    name="size"
                    value={size}
                    checked={selectedSize === size}
                    onChange={handleSizeChange}
                  />
                  {size}
                </label>
              ))}
              <div className="flex items-center mt-5">
                <button onClick={() => setIsSizeGuideOpen(true)} className="flex items-center text-sm">
                <span className="mr-3 text-base"><LiaRulerHorizontalSolid /> </span> Storleksguide 
                </button>
              </div>
              {cartIsOpen && (
                <SlideOverCart cartItems={cartItems} products={products} closeCart={closeCart} count={count} increment={increment} decrement={decrement} calculateTotalQuantity={calculateTotalQuantity} calculateTotalPrice={calculateTotalPrice} calculateQuantities={calculateQuantities}/>
              )}
            </div>
          </div>
          <div className="mb-4">
            <label htmlFor="quantity" className="mr-2">Antal</label>
            <select
  id="quantity"
  value={selectedQuantity}
  onChange={handleQuantityChange}
  className="appearance-none bg-white border border-black rounded-full px-6 py-2"
>
  {quantities.map((quantity) => (
    <option
      key={quantity}
      value={quantity}
      className="bg-white rounded-full"
    >
      {quantity}
    </option>
  ))}
</select>
            <button className="mx-4 py-2 px-8 bg-black text-white rounded-full" onClick={() => addToCart(product.id)} >Lägg i varukorg</button>
          </div>
        </div>
      </div>

      <Modal isOpen={isSizeGuideOpen} onClose={() => setIsSizeGuideOpen(false)} />

    </div>

  );
};

export default Product;