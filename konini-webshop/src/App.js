import React, { useState, useEffect } from "react";
import { Routes, Route, useLocation } from "react-router-dom";
import axios from 'axios';
import ProductCatalogue from "./pages/ProductCatalogue";
import Navbar from "./components/navbar";
import Checkout from "./pages/Checkout";
import Home from "./pages/Home";
import Footer from "./components/Footer";
import Payment from "./pages/Payment";
import Product from "./pages/Product";
import NavBarCheckout from "./components/NavBarCheckout";
import Confirmation from "./pages/Confirmation";

function App() {
  const [cartIsOpen, setCartIsOpen] = useState(false);
  const [count, setCount] = useState(0);
  const [cartItems, setCartItems] = useState([]);
  const [products, setProducts] = useState([]);

  const location = useLocation();

  const addToCart = (productId) => {
    setCartItems([...cartItems, productId]);
    setCartIsOpen(true);
  };


  const increment = () => {
    setCount(prevCount => {
      console.log('Incrementing count:', prevCount + 1);
      return prevCount + 1;
    });
  };

  const decrement = () => {
    setCount(prevCount => {
      const newCount = prevCount > 0 ? prevCount - 1 : 0;
      console.log('Decrementing count:', newCount);
      return newCount;
    });
  };

  const calculateQuantities = () => {
    const quantities = {};
    cartItems.forEach(itemId => {
      quantities[itemId] = (quantities[itemId] || 0) + 1;
    });
    return quantities;
  };

  const quantities = calculateQuantities();

  const calculateTotalQuantity = (cartItemId, count) => {
    return quantities[cartItemId] + count;
  };

  const calculateTotalPrice = (cartItemId, count, product) => {
    return (quantities[cartItemId] + count) * product.price;
  };


  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/products.json');
        setProducts(response.data.products);
      } catch (error) {
        console.error('Error fetching products:', error.message);
      }
    };

    fetchData();
  }, []);

  const isPaymentOrCheckout = location.pathname.includes("/Payment") || location.pathname.includes("/Checkout");

  return (
    <div>
      {!isPaymentOrCheckout && <Navbar setCartIsOpen={setCartIsOpen} />}
      {isPaymentOrCheckout && <NavBarCheckout />} {/* Render NavBarCheckout only on Checkout and Payment pages */}
      <Routes>
        <Route path="/"
          element={<Home
            cartIsOpen={cartIsOpen}
            setCartIsOpen={setCartIsOpen} />} />

        <Route path="/Product/:productId"
          element={<Product
            cartIsOpen={cartIsOpen}
            setCartIsOpen={setCartIsOpen}
            count={count}
            products={products}
            cartItems={cartItems}
            setCartItems={setCartItems}
            calculateTotalQuantity={calculateTotalQuantity}
            calculateTotalPrice={calculateTotalPrice}
            calculateQuantities={calculateQuantities} />} />

        <Route path="/ProductCatalogue"
          element={<ProductCatalogue
            addToCart={addToCart}
            cartIsOpen={cartIsOpen}
            setCartIsOpen={setCartIsOpen}
            count={count} increment={increment}
            decrement={decrement}
            products={products}
            cartItems={cartItems}
            setCartItems={setCartItems}
            calculateTotalQuantity={calculateTotalQuantity}
            calculateTotalPrice={calculateTotalPrice}
            calculateQuantities={calculateQuantities} />
          } />

        <Route path="/Checkout"
          element={<Checkout
            addToCart={addToCart}
            count={count}
            increment={increment}
            decrement={decrement}
            calculateTotalQuantity={calculateTotalQuantity}
            calculateTotalPrice={calculateTotalPrice}
            calculateQuantities={calculateQuantities} />}
        />
        <Route path="/Payment" element={<Payment />} />

        <Route path="/Confirmation" element={<Confirmation />} />
      </Routes>
      {!isPaymentOrCheckout && <Footer />}
    </div>
  );
}

export default App;