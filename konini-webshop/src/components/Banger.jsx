import React from 'react';
import BangerImg1 from '../assets/banger-img-1.png';
import BangerImg2 from '../assets/banger-img-2.png';

const Banger = () => {
    return (
        <div className="flex flex-wrap my-10">
        <div className="relative flex-shrink-0 w-full sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2">
            <img src={BangerImg1} alt="1" className="w-full h-auto" />
            <div className="absolute bottom-0 left-0 p-4 text-white">
                <a href="#" className="text-xs sm:text-sm underline">Klänningar</a>
            </div>
        </div>
        <div className="relative flex-shrink-0 w-full sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2">
            <img src={BangerImg2} alt="2" className="w-full h-auto" />
            <div className="absolute bottom-0 left-0 p-4 text-white">
                <a href="#" className="text-xs sm:text-sm underline">Blusar ooch skjortor</a>
            </div>
        </div>
    </div>
    
    );
};

export default Banger;
