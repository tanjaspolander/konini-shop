import React, { useState } from 'react';

const Incrementer = ({ handleClick, title }) => {


  return (
      <button
        className="bg-transparent hover:bg-gray-100 hover:border-gray-300 text-black px-2 mr-2 border border-gray-300 hover:border-transparent rounded"
        onClick={handleClick}
      >
        { title }
      </button>

  );
};

export default Incrementer;

