import React from 'react';
import Jenny from '../assets/jenny.png';
import Ranias from '../assets/ranias.png';
import Soraya from '../assets/soraya.png';
import Person from '../assets/person.png';
import Insta1 from '../assets/insta1.png';
import Insta2 from '../assets/insta2.png';
import Insta3 from '../assets/insta3.png';
import Insta4 from '../assets/insta4.png';

const Aside = () => {
    return (
        <div className=''>
            <div className="relative pb-10 border-t-2 border-black py-10">
                <div className="mx-auto max-w-6xl grid gap-3 mt-20 mb-20">
                    <h3 className="col-span-full text-2xl font-semibold">Följ oss</h3>
                    <p className="col-span-full">Se våra senaste inlägg på Instagram</p>
                    <div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-4 gap-3">
                        <img src={Insta1} alt="1" className="w-full h-auto" />
                        <img src={Insta2} alt="2" className="w-full h-auto" />
                        <img src={Insta3} alt="3" className="w-full h-auto" />
                        <img src={Insta4} alt="4" className="w-full h-auto" />
                    </div>
                </div>
            </div>
            <div className="relative pb-10 border-t-2 py-10">
                <div className="mx-auto max-w-6xl grid gap-3 mt-20 mb-20">
                    <h3 className="col-span-full text-2xl font-semibold">Spanat</h3>
                    <p className="col-span-full">Få inspiration från andra kvinnor</p>
                    <div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-4 gap-3">
                        <div className='flex flex-col'>
                            <img src={Ranias} alt="Ranias" className="w-full h-auto" />
                            <h3 className='text-base font-semibold py-1'>Ranias Hemoun</h3>
                            <h4 className='text-sm py-1'>Blus Namn 499 kr</h4>
                            <p className='text-xs py-1'>MODSTRÖM</p>
                        </div>
                        <div className='flex flex-col'>
                            <img src={Jenny} alt="Jenny" className="w-full h-auto" />
                            <h3 className='text-base font-semibold py-1'>Jenny Strömstedt</h3>
                            <h4 className='text-sm py-1'>Blus Namn 499 kr</h4>
                            <p className='text-xs py-1'>MODSTRÖM</p>
                        </div>
                        <div className='flex flex-col'>
                            <img src={Soraya} alt="Soraya" className="w-full h-auto" />
                            <h3 className='text-base font-semibold py-1'>Soraya Lavasani</h3>
                            <h4 className='text-sm py-1'>Blus Namn 499 kr</h4>
                            <p className='text-xs py-1'>MODSTRÖM</p>
                        </div>
                        <div className='flex flex-col'>
                            <img src={Person} alt="Person" className="w-full h-auto" />
                            <h3 className='text-base font-semibold py-1'>Anna Sarlvit</h3>
                            <h4 className='text-sm py-1'>Blus Namn 499 kr</h4>
                            <p className='text-xs py-1'>MODSTRÖM</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Aside;
