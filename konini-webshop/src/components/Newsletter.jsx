import React from 'react';
import { FaAngleRight } from "react-icons/fa6";

const Newsletter = () => {
    return (
        <div className="grid grid-cols-1 sm:grid-cols-1 lg:grid-cols-2 border-t-2 border-black">
            <div className="grid bg-white p-4 sm:col-span-1 md:col-span-1 lg:col-span-1 lg:border-r-2 border-black">
                <div className='p-4 sm:p-8'>
                    <h3 className="text-2xl font-bold">Osäker på din storlek?</h3>
                    <p>Vi hjälper dig med en avancerad storleksguide!</p>
                    <div className='mt-4 sm:mt-8'>
                        <div className="flex items-center mb-2">
                            <a href="#" className="text-black block">Underdelar</a>
                            <FaAngleRight className="text-xs mx-2" />
                        </div>
                        <div className="flex items-center mb-2">
                            <a href="#" className="text-black block">Överdelar</a>
                            <FaAngleRight className="text-xs mx-2" />
                        </div>
                        <div className="flex items-center">
                            <a href="#" className="text-black block">Hela storleksguiden</a>
                            <FaAngleRight className="text-xs mx-2" />
                        </div>
                    </div>
                </div>
            </div>
            <div className='bg-peach text-center sm:col-span-2 lg:col-span-1'>
                <div className="p-4 sm:p-8">
                    <h3 className="text-lg font-bold">Håll dig uppdaterad!</h3>
                    <p className='mb-5'>Signa upp till vårt nyhetsbrev och få exklusiva erbjudanden och nyheter skickat till dig.</p>
                    <form className="max-w-sm mx-auto">
                        <div className="mb-5">
                            <input
                                type="password"
                                id="repeat-password"
                                placeholder='E-post'
                                className="shadow-sm bg-white border border-gray-300 text-gray-900 text-sm rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                required
                            />
                        </div>
                        <button
                            type="submit"
                            className="text-white bg-black focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center w-full dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                            Register new account
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Newsletter;

