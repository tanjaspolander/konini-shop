import React from 'react';
import { AiOutlineClose } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import Incrementer from './Incementer';

const SlideOverCart = ({ cartItems, products, closeCart, count, increment, decrement, calculateTotalPrice, calculateTotalQuantity, calculateQuantities}) => {
  const navigate = useNavigate();

  

  const quantities = calculateQuantities();



  const handleCheckout = () => {
    const selectedProducts = cartItems.reduce((acc, itemId) => {
      const existingProduct = acc.find(item => item.product.id === itemId);
      if (existingProduct) {
        existingProduct.quantity += 1;
      } else {
        const product = products.find(product => product.id === itemId);
        acc.push({ product, quantity: 1 });
      }
      return acc;
    }, []);
    navigate('/Checkout', { state: { selectedProducts } });
  };



  // Check if the cart is empty and don't render if it is empty
  if (cartItems.length === 0) {
    return null;
  }

  return (
    <>
      <div className="fixed right-0 top-24 pt-4 w-[40%] h-full border-l-2 bg-white ease-in-out duration-500 overflow-y-auto">
        <div className="p-4">
          <button >
            <AiOutlineClose onClick={closeCart} />
          </button>
          <h2 className="text-2xl font-semibold mb-4">Shopping Cart</h2>
          <ul className="divide-y divide-gray-200">
            {Object.keys(quantities).map((cartItemId, index) => {
              const product = products.find((p) => p.id === parseInt(cartItemId, 10));

              if (product) {
                const totalQuantity = calculateTotalQuantity(cartItemId, count);
                const totalPrice = calculateTotalPrice(cartItemId, count, product);
                return (
                  <li key={index} className="py-4">
                    <div className="flex items-start justify-between">
                      <div className="flex-shrink-0">
                        <img className="h-12 w-12" src={product.images[1]} alt={product.name} />
                      </div>
                      <div className="ml-4 flex-1 flex flex-col">
                        <div>
                          <div className="flex justify-between text-base font-medium text-gray-900">
                            <h3>
                              <a href={product.href}>{product.name}</a>
                            </h3>
                            <p className="ml-4">
                            <Incrementer
                                
                                handleClick={increment}
                                title="+"
    
                              />
                              <Incrementer 
                              
                              handleClick={decrement}
                              title="-"/>
                            </p>
                          </div>
                          <p className="mt-1 text-sm text-gray">{product.color}</p>
                        </div>
                        <div className="flex flex-1 items-end justify-between text-sm">
                        <p className="text-gray">Antal {totalQuantity}</p>

                        <p className="text-black text-lg">{totalPrice}kr</p>
                        </div>
                      </div>
                    </div>
                  </li>
                );
              }
              return null;
            })}
          </ul>
          <div className="flex justify-between items-center mt-4">
            <p className="text-lg font-semibold">Total:</p>
            {/* byt ut med en counter för alla produkter i korgen */}
            <p className="text-xl">kr</p>
          </div>
          <button onClick={handleCheckout} className="bg-dark-pink text-white rounded-full  tracking-widest py-2 w-full">GÅ TILL KASSAN</button>

        </div>
      </div>
    </>
  );
};

export default SlideOverCart;