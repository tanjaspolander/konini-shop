import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { LiaShoppingBagSolid } from "react-icons/lia";
import { TbSearch } from "react-icons/tb";
import { LuUser } from "react-icons/lu";
import Logo from '../assets/logo.png';
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai';


const Navbar = ({ setCartIsOpen }) => {
    const [mobileNavIsOpen, setmobileNavIsOpen] = useState(false);
    const toggleNav = () => {
        setmobileNavIsOpen(!mobileNavIsOpen);
    };

    const toggle = () => {
        setCartIsOpen(prevState => !prevState);
    }


    const navLinks = [
        { id: 1, text: 'Shop', to: "/ProductCatalogue" },
        { id: 2, text: 'Varumärken', to: "#" },
        { id: 3, text: 'Nyheter', to: "#" }
    ];

    return (
        <div className="sticky top-0 z-50 border-b-2 border-black">
            <div className='w-full bg-dark-pink'>
                <h1 className="text-m py-1 text-white text-center tracking-wider">Fri frakt över 500:-</h1>
            </div>
            <nav className="bg-white p-4">

                <div className="container flex m-auto items-center justify-between">
                    <div className="self-start">

                        <Link to="/">
                            <img src={Logo} alt="Logo" className="h-10 object-contain" />
                        </Link>
                    </div>
                    <ul className='hidden md:flex'>
                        {navLinks.map(item => (
                            <li key={item.id} className='p-4 duration-300 hover:text-gray-600 cursor-pointer border-gray-400'>
                                <Link  to={item.to}>
                                    {item.text}
                                </Link>
                            </li>
                        ))}
                    </ul>
                    <div onClick={toggleNav} className='block md:hidden'>
                        {mobileNavIsOpen ? <AiOutlineClose className='text-neutral-900' size={20} /> : <AiOutlineMenu className='text-neutral-900' size={20} />}
                    </div>
                    <ul className={
                            mobileNavIsOpen
                                ? 'fixed md:hidden left-0 top-0 w-[40%] h-full border-r border-r-gray-900 bg-white ease-in-out duration-500'
                                : 'ease-in-out w-[60%] duration-500 fixed top-0 bottom-0 left-[-100%]'
                        }
                    >
                        <img src={Logo} alt="Logo" className="m-5 h-10 w-auto mr-2 object-contain" />
                        {navLinks.map(item => (
                            <li className='p-3 duration-300 hover:text-gray-600 cursor-pointer'>
                                <Link key={item.id} to={item.to}>
                                    {item.text}
                                </Link>
                            </li>
                        ))}
                    </ul>
                    <div className="flex space-x-6">
                        <TbSearch className="text-neutral-900 cursor-pointer" size={20} />
                        <LuUser className="text-neutral-900 cursor-pointer" size={20} />

                        <button aria-label="cart" className="text-neutral-900 cursor-pointer">
                            <LiaShoppingBagSolid size={22} onClick={() => toggle()} />
                        </button>

                    </div>
                </div>
            </nav>
        </div>
    );
};

export default Navbar;




