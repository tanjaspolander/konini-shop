import headerImage from '../assets/header.png';

const Header = () => {
    return (
        <header className="relative pb-10 border-b-2 border-black ">
        <img src={headerImage} alt="Header" className="w-full h-auto" />
        <div className="absolute bottom-0 right-0 p-4 text-white max-w-sm mb-24">
            <h1 className="text-2xl sm:text-3xl md:text-4xl lg:text-5xl font-bold mb-4">Nyheter inför sommaren</h1>
            <button className="rounded-full bg-dark-pink text-white text-xs sm:text-sm tracking-widest px-12 py-2 uppercase ">Utforska nyheter</button>
        </div>
    </header>
    );
};

export default Header;
