import React from 'react'
import { Link } from 'react-router-dom'
import LogoWhite from '../assets/logo-white.png'
const NavBarCheckout = () => {
  return (
    <div className="sticky top-0 z-50">
    <div className='w-full bg-coral'>
      <div className="flex items-center">
  
        <svg className="ml-10 w-6 h-6 text-white dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m15 19-7-7 7-7"/>
        </svg>
        <Link to={{ pathname: "/ProductCatalogue" }}>
          <h1 className="text-m py-10 text-white ">Fortsätt handla</h1>
        </Link>
        <img src={LogoWhite} alt="Logo" className="h-10 object-contain mx-auto " /> 
        
      </div>
    </div>
  </div>
  
  )
}

export default NavBarCheckout
