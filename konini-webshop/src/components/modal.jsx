import React from 'react';
import { AiOutlineClose } from 'react-icons/ai';


const Modal = ({ isOpen, onClose }) => {
    const sizeGuideData = [
      { size: 'XS', Byst: '78-81', Midja: '62-64', Stussvidd: '86-89', Axel: '38', Armlängd: '59', Innerbenslängd: '80', Kroppslängd: '160' },
      { size: 'S', Byst: '84-87', Midja: '68-70', Stussvidd: '92-95', Axel: '39', Armlängd: '60', Innerbenslängd: '81', Kroppslängd: '165' },
      { size: 'M', Byst: '90-93', Midja: '74-77', Stussvidd: '98-101', Axel: '40', Armlängd: '61', Innerbenslängd: '82', Kroppslängd: '170' },
      { size: 'L', Byst: '96-99', Midja: '80-84', Stussvidd: '104-107', Axel: '41', Armlängd: '62', Innerbenslängd: '83', Kroppslängd: '175' },
      { size: 'XL', Byst: '102-105', Midja: '86-90', Stussvidd: '110-113', Axel: '42', Armlängd: '63', Innerbenslängd: '84', Kroppslängd: '180' },
    ];
  
    const sizeColumns = Object.keys(sizeGuideData[0]).slice(1); // Exclude the 'size' property for columns
  
    return (
      <>
        {isOpen && (
          <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-50">
            <div className="bg-white border-solid border-2 border-black p-6">
                <div className="flex justify-between">
              <h2 className="text-xl font-bold mb-4">Storleksguide</h2>
              <AiOutlineClose onClick={onClose} className="cursor-pointer"/>
              </div>
              <table className="table-auto w-full">
                <thead>
                  <tr>
                    <th></th>
                    {sizeGuideData.map(({ size }) => (
                      <th key={size}>{size}</th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {sizeColumns.map((column) => (
                    <tr key={column}>
                      <td className="font-bold p-2">{column}</td>
                      {sizeGuideData.map(({ size, [column]: measurement }) => (
                        <td  className="text-sm px-5" key={`${size}-${column}`}>{measurement}</td>
                      ))}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        )}
      </>
    );
  };
  
  export default Modal;