import React from 'react';
import { FaAngleRight } from "react-icons/fa6";


const Info = () => {
    return (
        <div className='container mx-auto px-16'>
<div className="mx-auto max-w-7xl mb-24">
  <div className="flex flex-col items-center">
    <h2 className="text-3xl font-bold mb-4 text-center">Vad vi gör</h2>
    <p className="mb-4 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis nam molestias et. Dolor repellat culpa nesciunt suscipit itaque, fuga, pariatur dolore quas corporis molestias consectetur sit harum ex reiciendis tenetur?Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam vel, provident enim laborum iusto quia maiores dolor explicabo nostrum pariatur, earum impedit dolores ratione, ipsam repellat! Suscipit, sit quis. Architecto?Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur earum est ipsam nesciunt odit fugit eum laboriosam quasi, commodi iste optio, nihil magnam esse dolores! Saepe nulla recusandae soluta incidunt.</p>
  </div>
  <div className="flex items-center">
    <a href="#" className="text-xs sm:text-sm underline mr-2">Mer om oss</a>
    <FaAngleRight className="text-xs" />
  </div>
</div>
</div>

    );
};

export default Info;
