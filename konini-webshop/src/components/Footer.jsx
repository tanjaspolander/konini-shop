import React from 'react'

const Footer = () => {
  return (
    <footer className=" bg-black">
      <div className="mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8">
        <div className="flex flex-col md:flex-row justify-between items-center">
          <div className="grid grid-cols-1 gap-8 sm:gap-6 sm:grid-cols-5">
            <div>
              <h2 className="mb-6 text-sm font-semibold text-white ">Kontakt</h2>
              <ul className="text-white ">
                <li className="mb-4">
                  <a href="" className="hover:underline">Adressgatan 33</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Stockholm 99999</a>
                </li>
                <li>
                  <a href="" className="hover:underline">+46 666 666 66</a>

                </li>
                <li className="mt-4">
                  <a href="" className="hover:underline">Kontakta oss</a>

                </li>
              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-white  ">Kundtjänst</h2>
              <ul className="text-white ">
                <li className="mb-4">
                  <a href="" className="hover:underline">Storleksguide</a>
                </li>
                <li>
                  <a href="" className="hover:underline">FAQs</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Leverans</a>

                </li>
                <li className="mt-4">
                  <a href="" className="hover:underline">Byte och retur</a>

                </li>
              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-white ">Policyer</h2>
              <ul className="text-white ">
                <li className="mb-4">
                  <a href="" className="hover:underline">Privacy policies</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Villkår</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Cookies</a>

                </li>

              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-white ">Om oss</h2>
              <ul className="text-white ">
                <li className="mb-4">
                  <a href="" className="hover:underline">Vad vi gör</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Hållbarhet</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Cookies</a>

                </li>

              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-white ">Följ oss</h2>
              <ul className="text-white ">
                <li className="mb-4">
                  <a href="" className="hover:underline">Instagram</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Facebook</a>
                </li>
                <li>
                  <a href="" className="hover:underline">Nyhetsbrev</a>

                </li>

              </ul>
            </div>

          </div>
        </div>
        <hr className="my-6 border-white sm:mx-auto dark:border-gray-700 lg:my-8" />
        <div className="sm:flex sm:items-center sm:justify-between">
          <span className="text-sm text-white sm:text-center">© 2023 <a href="" className="hover:underline">Konini™</a>. All Rights Reserved.
          </span>
        </div>
      </div>
    </footer>
  )
}

export default Footer