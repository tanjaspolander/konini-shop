/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {},
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'white': '#ffffff',
      'dark-pink': '#D63D3D',
      'coral': '#F85F5F',
      'peach': '#FFEADD',
      'black': '#151515',
      'gray': '#6C6C6C',
      
    },
  },
  plugins: [],
}